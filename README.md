# PyMixage
Le projet consiste à concevoir un logiciel de mixage audio avec python.
Pendant cette semaine, on travaillera sur deux grands objectifs:
- Application de queleques effets et filtres sur un morceau audio choisi.
- Mixage de deux chansons ou plus en jouant sur quelques paramètres(volume, fréquences, tempo ...)

Les bibliothèques utilisées sont :
- tkinter (pour l'interface graphique)
- pygame.mixer (pour lire les fichiers audio)
- librosa (pour analyser les musiques et appliquer certains effets)
- numpy (pour manipuler les musiques)
- scipy (pour appliquer certains effets)
- soundfile (pour sauvegarder les musiques en .wav)
- pydub (pour convertir les fichiers .mp3 en fichiers .wav)
- PIL (pour ouvrir des images dans l'interface)
- re (pour certaines manipulations d'input)