import librosa
import numpy as np
from scipy import signal
import soundfile as sf

def bass_boost(filename) :                                   #a function that implements a bass boost effect to a song
    y, sr = librosa.load(filename)                           #loading the audio as a time series np array y / sr represents sampling rate
    b, a = signal.butter(3, 0.1)
    zi = signal.lfilter_zi(b, a)                             #applying the filter several times to get higher order of low pass
    z, _ = signal.lfilter(b, a, y, zi=zi*1)
    z2, _ = signal.lfilter(b, a, z, zi=zi*1)
    ymod = signal.filtfilt(b, a, y)                          #applying a low pass filter to the original song
    index = filename.rfind('.')
    new_filename = filename[:index]+'_bass_boosted.wav'      #attributing the filename with its directory
    sf.write(new_filename, (y+ymod), sr, subtype='PCM_16')   #converting the np.array (y+y_mod) to an audio file
    return new_filename
