import librosa
import numpy as np
import soundfile as sf

def separate(filename):                                           #function that seperates the harmonic and percussive part of an audio file and recombines them with an amplification of the percussive part
    y, sr = librosa.load(filename)                                #loading the audio as a time series np array y / sr represents sampling rate
    y_harmonic = librosa.effects.harmonic(y,margin=3.0)           #extracting the harmonic part
    y_percussive = librosa.effects.percussive(y, margin=3.0)      #extracting the percussive part
    index = filename.rfind('.')
    new_filename_h = filename[:index]+'_harmonic.wav'
    new_filename_p = filename[:index]+'_percussive.wav'
    sf.write(new_filename_h,y_harmonic , sr, subtype='PCM_16')   # converting y_harmonic to an audio file
    sf.write(new_filename_p,y_percussive , sr, subtype='PCM_16') # converting y_percussive to an audio file
    return new_filename_h,new_filename_p
