import pydub

def mp3_to_wav(filepath) : #This function writes a .wav file from a .mp3 file
    sound = pydub.AudioSegment.from_mp3(filepath) #loading the .mp3 file
    index = filepath.rfind('.') #Naming the new file by replacing "".mp3" with ".wav"
    new_path = filepath[:index]+'.wav'
    sound.export(new_path, format="wav") #Exporting the .mp3 file