import librosa
import numpy as np
import soundfile as sf
import pygame.mixer


def save_song(list_song, list_volume, name) : #This function save the mix
    y = np.array([])
    list_length = []
    for k in range(len(list_song)) : #Making a lists of the length of all the songs to determine which one is the longest
        list_length.append(len(librosa.load(list_song[k])[0]))
    length = max (list_length)
    save_file = np.zeros(length) #The saved file will be the same length as the longest song in the mix
    for k in range(len(list_song)) :
        y, sr = librosa.load(list_song[k]) #Loading each song and adding it in the saved file with the volume set up by the user
        for i in range(len(y)) :
            save_file[i] = save_file[i] + y[i]*list_volume[k]
    sf.write(name, save_file, sr, subtype='PCM_16') #Writing the file
