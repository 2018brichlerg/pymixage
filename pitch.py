import librosa
import numpy as np
import soundfile as sf

def pitch_shifting(filename,nsteps):                                 #a function that enables pitch_shifting by modifying the number of half steps
    y, sr = librosa.load(filename)                                   #loading the audio as a time series np array y / sr represents sampling rate
    y_shift = librosa.effects.pitch_shift(y, sr, n_steps=nsteps)     #pitch shifting the audio by n_steps half steps
    index = filename.rfind('.')
    new_filename = filename[:index]+'_pitch_shifted.wav'             #attributing the filename with its directory
    sf.write(new_filename,y_shift , sr, subtype='PCM_16')            #converting the np.array y_shift to an audio file
    return new_filename

