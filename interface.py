﻿import tkinter
from tkinter.filedialog import askopenfilename, askdirectory
from song_play import *
from export import mp3_to_wav
import filters
from PIL import Image, ImageTk
import synchronisation
import pitch
import save_result
import separation
import pygame.mixer
import re

#################################   Fonctions   #################################

def remove_duplicates(list) :     # Cette fonction enlève les éléments redondants dans une liste
    
    new_list = []
    for element in list :
        if element not in new_list :
            new_list.append(element)
    return new_list


def tk_read_file(n) :             # Cette fonction permet de sélectionner le fichier à analyser

    global file_path0
    global file_path1
    global file_path2
    global file_path3

    
    if n==0 :
        global file_path0
        input_file_path = askopenfilename(parent=root, title="Select File") 
                                                         # Ouvre l'explorateur de fichiers
        file_path0 = input_file_path.rstrip('\n')        # Prend la valeur de l'emplacement du fichier
        index = file_path0.rfind('/')
        index_type = file_path0.rfind('.')
        if(file_path0[index_type:] == '.mp3') :          #change mp3 file to wav file
            mp3_to_wav(file_path0)
            file_path0 = file_path0[:index_type]+'.wav'
        load_song(file_path0, 0)                         #load the music file
        load_song(filters.bass_boost(filename = file_path0), 4)
                                                         #load bass boost in another channel
        title = file_path0[index+1:]
        selected_channel0.set(title)                     #show file name in the interface
    if n==1 :
        global file_path1
        input_file_path = askopenfilename(parent=root, title="Select File") 
                                                         # Ouvre l'explorateur de fichiers
        file_path1 = input_file_path.rstrip('\n')        # Prend la valeur de l'emplacement du fichier
        index = file_path1.rfind('/')
        index_type = file_path1.rfind('.')
        if(file_path1[index_type:] == '.mp3') :          #change mp3 file to wav file
            mp3_to_wav(file_path1)
            file_path1 = file_path1[:index_type]+'.wav'
        load_song(file_path1, 1)                         #load the music file
        load_song(filters.bass_boost(filename = file_path1), 5)
                                                         #load bass boost in another channel
        title = file_path1[index+1:]
        selected_channel1.set(title)                     #show file name in the interface
        print(file_path1)
    if n==2 :
        global file_path2
        input_file_path = askopenfilename(parent=root, title="Select File") 
                                                         # Ouvre l'explorateur de fichiers
        file_path2 = input_file_path.rstrip('\n')        # Prend la valeur de l'emplacement du fichier
        index = file_path2.rfind('/')
        index_type = file_path2.rfind('.')
        if(file_path2[index_type:] == '.mp3') :          #change mp3 file to wav file
            mp3_to_wav(file_path2)
            file_path2 = file_path2[:index_type]+'.wav'
        load_song(file_path2, 2)                         #load the music file
        load_song(filters.bass_boost(filename = file_path2), 6)
                                                         #load bass boost in another channel
        title = file_path2[index+1:]
        selected_channel2.set(title)                     #show file name in the interface
    if n==3 :
        global file_path3
        input_file_path = askopenfilename(parent=root, title="Select File") 
                                                         # Ouvre l'explorateur de fichiers
        file_path3 = input_file_path.rstrip('\n')        # Prend la valeur de l'emplacement du fichier
        index = file_path3.rfind('/')
        index_type = file_path3.rfind('.')
        if(file_path3[index_type:] == '.mp3') :          #change mp3 file to wav file
            mp3_to_wav(file_path3)
            file_path3 = file_path3[:index_type]+'.wav'
        load_song(file_path3, 3)                         #load the music file
        load_song(filters.bass_boost(filename = file_path3), 7)
                                                         #load bass boost in another channel
        title = file_path3[index+1:]
        selected_channel3.set(title)                     #show file name in the interface

def pause_unpause(n) :                                   
                                  #this function pause and unpause the music on channel n
    print(volume0.get())
    if n == 0 :
        if status0.get() == True :                       #check if a song is playing
            if pause_song(n):
                status0.set(False)
                print("pause")
            else:
                print("no file in track")
        else :
            if resume_song(n):
                status0.set(True)
                print("unpause")
            else:
                print("no file in track")
    if n == 1 :
        if status1.get() == True :                       #check if a song is playing
            if pause_song(n):
                status1.set(False)
                print("pause")
            else:
                print("no file in track")
        else :
            if resume_song(n):
                status1.set(True)
                print("unpause")
            else:
                print("no file in track")
    if n == 2 :
        if status2.get() == True :                       #check if a song is playing
            if pause_song(n):
                status2.set(False)
                print("pause")
            else:
                print("no file in track")
        else :
            if resume_song(n):
                status2.set(True)
                print("unpause")
            else:
                print("no file in track")
    if n == 3 :
        if status3.get() == True :                       #check if a song is playing
            if pause_song(n):
                status3.set(False)
                print("pause")
            else:
                print("no file in track")
        else :
            if resume_song(n):
                status3.set(True)
                print("unpause")
            else:
                print("no file in track")
    if((status0.get() or status1.get() or status2.get() or status3.get())==False):
                                                         #check and set the master status 
        status_all.set(False)

def play(n,volumn,sys_volumn,volumn_boost) :
                                            #this function starts a song loaded to the given channel 
                                            #with given volume, effect volume and system volume                                            
    if start_song(n,volumn,sys_volumn,volumn_boost):
        if n ==0 :
            status0.set(True)
            status_all.set(True)
        if n==1 :
            status1.set(True)
            status_all.set(True)
        if n==2 :
            status2.set(True)
            status_all.set(True)
        if n==3 :
            status3.set(True)
            status_all.set(True)
    print(status0.get(),status1.get(),status2.get(),status3.get())

def channel_repeat(n):  
                                            #to decide if the song losded in the given channel will repeat
    song_repeat(n)
    print()
        
def start_all(sys_volumn):                  #start all songs if loaded in a channel
    for i in range(10):
        if(pygame.mixer.Channel(i).get_busy()):                     #check if a song is loaded in the given channel
            pygame.mixer.Channel(i).set_volume(pygame.mixer.Channel(i).get_volume()*sys_volumn)
            pygame.mixer.Channel(i).unpause()
            if i ==0 :                      #start with the volume information
                set_volumn_song(0,int(volume0.get())/100.,master_volume.get()/100.)
            if i ==1 :
                set_volumn_song(1,int(volume1.get())/100.,master_volume.get()/100.)
            if i ==2 :
                set_volumn_song(2,int(volume2.get())/100.,master_volume.get()/100.)
            if i ==3 :
                set_volumn_song(3,int(volume3.get())/100.,master_volume.get()/100.)
            if i == 4:
                set_volumn_song(4,int(volume4.get())/100.,master_volume.get()/100.)
            if i ==5 :
                set_volumn_song(5,int(volume5.get())/100.,master_volume.get()/100.)
            if i ==6 :
                set_volumn_song(6,int(volume6.get())/100.,master_volume.get()/100.)
            if i ==7 :
                set_volumn_song(7,int(volume7.get())/100.,master_volume.get()/100.)
    
def set_volume0(volume) :                       #set the volume of channel 0 with changement of the volume scale
    set_volumn_song(0,float(volume)/100.,master_volume.get()/100.)
    
def set_volume1(volume) :                       #set the volume of channel 1 with changement of the volume scale
    set_volumn_song(1,float(volume)/100.,master_volume.get()/100.)

def set_volume2(volume) :                       #set the volume of channel 2 with changement of the volume scale
    set_volumn_song(2,float(volume)/100.,master_volume.get()/100.)

def set_volume3(volume) :                       #set the volume of channel 3 with changement of the volume scale
    set_volumn_song(3,float(volume)/100.,master_volume.get()/100.)

def set_volume4(volume) :                       #set the volume of channel 4(bass boost) with changement of the volume scale
    set_volumn_song(4,float(volume)/100.,master_volume.get()/100.)
    
def set_volume5(volume) :                       #set the volume of channel 5(bass boost) with changement of the volume scale
    set_volumn_song(5,float(volume)/100.,master_volume.get()/100.)
    
def set_volume6(volume) :                       #set the volume of channel 6(bass boost) with changement of the volume scale
    set_volumn_song(6,float(volume)/100.,master_volume.get()/100.)
    
def set_volume7(volume) :                       #set the volume of channel 7(bass boost) with changement of the volume scale
    set_volumn_song(7,float(volume)/100.,master_volume.get()/100.)

def bass_boost(channel,bass_boost):             #start bass boost of the given channel
    print(channel,bass_boost)


def bass_boost1(channel,bass_boost):
    print("ok")

def synchro(select_synchro0, select_synchro1, select_synchro2, select_synchro3):
                                                #define how 2 channels synchronize with each other
    n1=-1
    n2=-1
    if select_synchro0.get() != "channel __ " :
        string1 = str(select_synchro0.get())
        n1 = int(re.search(r'\d+', string1).group())            #Getting any int in the select_synchro entry
        n2 = 0
    if select_synchro1.get() != "channel __ " :
        string1 = str(select_synchro1.get())
        n1 = int(re.search(r'\d+', string1).group())
        n2 = 1
    if select_synchro2.get() != "channel __ " :
        string1 = str(select_synchro2.get())
        n1 = int(re.search(r'\d+', string1).group())
        n2 = 2
    if select_synchro3.get() != "channel __ " :
        string1 = str(select_synchro3.get())
        n1 = int(re.search(r'\d+', string1).group())
        n2 = 3
    if n1 == 0:
        filename1 = file_path0
    if n1 == 1:
        filename1 = file_path1
    if n1 == 2:
        filename1 = file_path2
    if n1 == 3:
        filename1 = file_path3
    if n2 == 0:
        filename2 = file_path0
    if n2 == 1:
        filename2 = file_path1
    if n2 == 2:
        filename2 = file_path2
    if n2 == 3:
        filename2 = file_path3
    if n1 != -1 :
        synchronisation.synchro_file(filename1,filename2,n2)
    

def set_volumn_all(volume) :                                    #set volume of all songs with the given system volume
    for k in range(10) :
        set_volumn_song(k,volume0.get()/100.,float(volume)/100.)

def set_all_start():                                            #start all songs with the function defined above
    start_all(master_volume.get()/100.)
    status_all.set(True)
    status0.set(if_loaded(0))
    status1.set(if_loaded(1))
    status2.set(if_loaded(2))
    status3.set(if_loaded(3))
    
def set_all_pause_unpause():                                    #pause or unpause all songs
    if status_all.get()==False:                                 #if no song is playing
        resume_all()
        status_all.set(True)
        if if_loaded(0):
            status0.set(True)
        if if_loaded(1):
            status1.set(True)
        if if_loaded(2):
            status2.set(True)
        if if_loaded(3):
            status3.set(True)
    else:                                                       #if some songs ara playing
        pause_all()
        status_all.set(False)
        status0.set(False)
        status1.set(False)
        status2.set(False)
        status3.set(False)
        
def set_all_stop():                                             #stop all songs
    stop_all()
    status0.set(False)
    status1.set(False)
    status2.set(False)
    status3.set(False)

def pitch_selected(n,if_pitch,pitch_value) :               
                                        #set pitch information with the given channel, status and value information
    if if_pitch :                       #if the given channel is selected to switch pitch
        if n==0 :
            new_filepitch=pitch.pitch_shifting(file_path0, pitch_value.get())
            load_song(new_filepitch, 0) #load pitch file for this channel
            load_song(filters.bass_boost(filename = new_filepitch), 4)
        if n==1 :
            new_filepitch=pitch.pitch_shifting(file_path1, pitch_value.get())
            load_song(new_filepitch, 1) #load pitch file for this channel
            load_song(filters.bass_boost(filename = new_filepitch), 5)
        if n==2 :
            new_filepitch=pitch.pitch_shifting(file_path2, pitch_value.get())
            load_song(new_filepitch, 2) #load pitch file for this channel
            load_song(filters.bass_boost(filename = new_filepitch), 6)
        if n==3 :
            new_filepitch=pitch.pitch_shifting(file_path3, pitch_value.get())
            load_song(new_filepitch, 3) #load pitch file for this channel
            load_song(filters.bass_boost(filename = new_filepitch), 7)

def song_repeat(n):
                                        #to decide if a channel n repeats the song loaded in it
    if n == 0 :
        r0 = repeat_bool0               #r0 the status of wether a song rapeats
        if r0.get():
            pygame.mixer.Channel(n).play(pygame.mixer.Channel(n).get_sound(),-1)
        
        else:
            song = pygame.mixer.Channel(n).get_sound()
            pygame.mixer.Channel(n).stop()
            pygame.mixer.Channel(n).play(song, loops=0)
    if n == 1 :
        r0 = repeat_bool1               #r0 the status of wether a song rapeats
        if r0.get():
            pygame.mixer.Channel(n).play(pygame.mixer.Channel(n).get_sound(),-1)
        
        else:
            song = pygame.mixer.Channel(n).get_sound()
            pygame.mixer.Channel(n).stop()
            pygame.mixer.Channel(n).play(song, loops=0)
    if n == 2 :
        r0 = repeat_bool2               #r0 the status of wether a song rapeats
        if r0.get():
            pygame.mixer.Channel(n).play(pygame.mixer.Channel(n).get_sound(),-1)
        
        else:
            song = pygame.mixer.Channel(n).get_sound()
            pygame.mixer.Channel(n).stop()
            pygame.mixer.Channel(n).play(song, loops=0)
    if n == 3 :
        r0 = repeat_bool3               #r0 the status of wether a song rapeats
        if r0.get():
            pygame.mixer.Channel(n).play(pygame.mixer.Channel(n).get_sound(),-1)
        
        else:
            song = pygame.mixer.Channel(n).get_sound()
            pygame.mixer.Channel(n).stop()
            pygame.mixer.Channel(n).play(song, loops=0)
    
def list_songs() :                      #to save the information of all songs in all channel
    list_files = []                     #list of file path
    list_volumes=[]                     #list of channel volume
    if pygame.mixer.Channel(0).get_busy():
        index0 = file_path0.rfind('.')
        new_filename0 = file_path0[:index0]+'_bass_boosted.wav'
        list_files.append(file_path0)
        list_volumes.append(float(volume0.get()/100.))
        list_files.append(new_filename0)
        list_volumes.append(float(volume4.get()/100.))
    if pygame.mixer.Channel(1).get_busy():
        index1 = file_path1.rfind('.')
        new_filename1 = file_path1[:index1]+'_bass_boosted.wav'
        list_files.append(file_path1)
        list_volumes.append(float(volume1.get()/100.))
        list_files.append(new_filename1)
        list_volumes.append(float(volume5.get()/100.))
    if pygame.mixer.Channel(2).get_busy():
        index2 = file_path2.rfind('.')
        new_filename2 = file_path2[:index2]+'_bass_boosted.wav'
        list_files.append(file_path2)
        list_volumes.append(float(volume2.get()/100.))
        list_files.append(new_filename2)
        list_volumes.append(float(volume6.get()/100.))
    if pygame.mixer.Channel(3).get_busy():
        index3 = file_path3.rfind('.')
        new_filename3 = file_path3[:index3]+'_bass_boosted.wav'
        list_files.append(file_path3)
        list_volumes.append(float(volume3.get()/100.))
        list_files.append(new_filename3)
        list_volumes.append(float(volume7.get()/100.))
        print(list_files)
    save_result.save_song(list_files, list_volumes, name_save.get())

def split_song(filename, n) :           #split a song into harmonic part and percussive part

    harmonic, percussive = separation.separate(filename)
    load_song(harmonic, n)
    load_song(filters.bass_boost(filename = harmonic), n+4)
    index = harmonic.rfind('/')
    title = harmonic[index+1:]
    if n == 0:
        selected_channel0.set(title)
        global file_path1
        file_path0 = harmonic
        file_path1 = percussive
    else :
        selected_channel2.set(title)
        global file_path3
        file_path2 = harmonic
        file_path3 = percussive        
    load_song(percussive,n+1)
    load_song(filters.bass_boost(filename = percussive), n+5)
    index = percussive.rfind('/')
    title = percussive[index+1:]
    if n == 0:
        selected_channel1.set(title)
    else :
        selected_channel3.set(title)

def quit(root) :                        # Cette fonction ferme la fenêtre
    stop_all()
    root.destroy()

#################################   Interface   #################################

root = tkinter.Tk()                     # Crée la fenêtre principale (root)
top = tkinter.Frame(root)               # use frame to overlap the widges
top.pack(side='top')

start_player()

root.minsize(400, 300)
root.geometry("1100x750")               # Configure la taille de root

root.title("Music player")

background_image=tkinter.PhotoImage(file='./background2.png') 
background_label = tkinter.Label(top, image=background_image) 
background_label.place(x=0, y=0, relwidth=1, relheight=1)
                                        # load the background image

################################################################################

################################################################################

status0 =tkinter.BooleanVar()           # the status of if a song is playing
status0.set(False) 

status1 =tkinter.BooleanVar()           # the status of if a song is playing
status1.set(False)

status2 =tkinter.BooleanVar()           # the status of if a song is playing
status2.set(False)

status3 =tkinter.BooleanVar()           # the status of if a song is playing
status3.set(False)

status_all =tkinter.BooleanVar()        # the status of if any song is playing
status_all.set(False)

################################### Channel 0 ###################################

select_channel0 = tkinter.Button(top, text = 'Select channel 0', command = lambda: tk_read_file(0))
select_channel0.config(height = 1, width = 17,bg='#9c9c9c')
select_channel0.grid(row = 1, column = 0, padx = 5, pady = 5)
                                        # define the select_file_path button

selected_channel0=tkinter.StringVar()
selected_channel0.set("")
selected_channel0_box=tkinter.Entry(top, width = 30, textvariable=selected_channel0,bg='#9c9c9c')
selected_channel0_box.grid(row=1, column=1, pady=5)
                                        # define the place to present the file path

repeat_bool0=tkinter.BooleanVar()
repeat_bool0.set(False)
check_repeat0=tkinter.Checkbutton(top,variable=repeat_bool0,text="repeat",command=lambda: channel_repeat(0))
check_repeat0.grid(row = 1, column = 2, padx = 5, pady = 10)
check_repeat0.config(bg='#9c9c9c')
                                        # define the status confirming checkbox of a song's repeating

button_pause0 = tkinter.Button(top, text = '⏸', command = lambda: pause_unpause(0))
button_pause0.config(height = 1, width = 2,bg='#9c9c9c')
button_pause0.grid(row = 1, column = 3, padx = 5, pady = 10)
                                        # define the pause and unpause button

button_play0 = tkinter.Button(top, text = '▶', command = lambda: play(0, volume0.get()/100., master_volume.get()/100., volume4.get()/100.))
button_play0.config(height = 1, width = 2,bg='#9c9c9c')
button_play0.grid(row = 1, column = 4, padx = 5, pady = 10)
                                        # define the start_playing button

button_rewind0 = tkinter.Button(top, text = '⏮', command = lambda: rewind_song(0,volume0.get()/100., master_volume.get()/100., volume4.get()/100.))
button_rewind0.config(height = 1, width = 2,bg='#9c9c9c')
button_rewind0.grid(row = 1, column = 5, padx = 5, pady = 10)
                                        # define the rewind button

volume0_grid=tkinter.Frame(top,bg='#9c9c9c')  
volume0_grid.grid(row=1,column=6)       # define the Frame for volume control of a perticular channel

volume0 = tkinter.DoubleVar()
volume_scale0 = tkinter.Scale(volume0_grid, variable = volume0, orient = tkinter.HORIZONTAL, command = set_volume0)
volume_scale0.grid(row = 1, column = 0, padx = 0, pady = 0)
volume0.set(100)
volume_scale0.config(bg='#9c9c9c')      # define the scale for volume in the Frame defined above
                     
volume0_label = tkinter.Label(volume0_grid, text='Volume channel 0')
volume0_label.grid(row=0, column=0, pady = 0)
volume0_label.config(bg='#9c9c9c')      # define the presenting box of volume information in the Frame defined above

check_pitch_shift0=tkinter.Frame(top)
check_pitch_shift0.grid(row=1,column=7)
check_pitch_shift0.config(bg='#9c9c9c') # define the Frame for pitch information
                          
pitch_value0=tkinter.IntVar()
pitch_scale0=tkinter.Scale(check_pitch_shift0, from_=-12,to=12,resolution=1,orient=tkinter.HORIZONTAL,variable=pitch_value0)
pitch_scale0.grid(row=1,column=0)
pitch_scale0.config(bg='#9c9c9c')
pitch_value0.set(0)                     # define the scale for pitch in the Frame defined above

if_pitch0=tkinter.BooleanVar()
pitch_button0=tkinter.Checkbutton(check_pitch_shift0,variable=if_pitch0, text="Pitch shift",command=lambda: pitch_selected(0,if_pitch0,pitch_value0))
pitch_button0.grid(row=0,column=0)
pitch_button0.config(bg='#9c9c9c')      # define the status_check button that wether a song to switch pitvh
                                        #in the Frame defined above

synch0_grid=tkinter.Frame(top,bg='#9c9c9c')
synch0_grid.grid(row=2,column=1)        # define the Frame for synchronising

synchro0_label = tkinter.Label(synch0_grid, text='Synchro with:')
synchro0_label.grid(row=0, column=0, pady = 0)
synchro0_label.config(bg='#9c9c9c')     # define the label to give hints in the Frame defined above

select_synchro0=tkinter.StringVar()
select_synchro0.set("channel __ ")
select_synchro0_box=tkinter.Entry(synch0_grid, width = 11, justify = tkinter.CENTER, textvariable=select_synchro0,bg='#9c9c9c')
select_synchro0_box.grid(row=0, column=1, pady=0)
                                        # define the entry box to input a channel number 
                                        # for this channel to synchronise with in the Frame defined above

volume4_grid=tkinter.Frame(top,bg='#9c9c9c')
volume4_grid.grid(row=2,column=6)       # define the volume Frame for the bass boost of channel0(0+4)

volume4_label = tkinter.Label(volume4_grid, text='Bass boost\nChannel 0')
volume4_label.grid(row=0, column=0, pady = 0)
volume4_label.config(bg='#9c9c9c')      # define the label to give information of bass boost

volume4 = tkinter.DoubleVar()
volumeboost0_scale = tkinter.Scale(volume4_grid, variable = volume4, orient = tkinter.HORIZONTAL, command = set_volume4)
volumeboost0_scale.grid(row = 0, column = 1, padx = 0, pady = 0)
volume4.set(0)
volumeboost0_scale.config(bg='#9c9c9c') # define the scale to control the volume of the bass boost

button_quit=tkinter.Button(top, text="Split harmonic\npercussive", command=lambda:split_song(file_path0,0))
button_quit.config(height = 2, width = 11,bg='#9c9c9c' )
button_quit.grid(row=1, column=8, padx=20,pady=5)
                                        # define a button to split a song
################################### space between 0&1 #####################################
space_channel01=tkinter.Label(top,bg='#707070')
space_channel01.grid(row=3)
################################### Channel 1 ###################################

select_channel1 = tkinter.Button(top, text = 'Select channel 1', command = lambda: tk_read_file(1))
select_channel1.config(height = 1, width = 17,bg='#9c9c9c')
select_channel1.grid(row = 4, column = 0, padx = 5, pady = 5)
                                        # define the select_file_path button
                                        
selected_channel1=tkinter.StringVar()
selected_channel1.set("")
selected_channel1_box=tkinter.Entry(top, width = 30, textvariable=selected_channel1,bg='#9c9c9c')
selected_channel1_box.grid(row=4, column=1, pady=5)
                                        # define the place to present the file path

repeat_bool1=tkinter.BooleanVar()
repeat_bool1.set(False)
check_repeat1=tkinter.Checkbutton(top,variable=repeat_bool1,text="repeat",command=lambda: channel_repeat(1),bg='#9c9c9c')
check_repeat1.grid(row = 4, column = 2, padx = 5, pady = 10)
                                        # define the status confirming checkbox of a song's repeating

button_pause1 = tkinter.Button(top, text = '⏸', command = lambda: pause_unpause(1),bg='#9c9c9c')
button_pause1.config(height = 1, width = 2)
button_pause1.grid(row = 4, column = 3, padx = 5, pady = 10)
                                        # define the pause and unpause button

button_play1 = tkinter.Button(top, text = '▶', command = lambda: play(1, volume1.get()/100., master_volume.get()/100., volume5.get()/100.))
button_play1.config(height = 1, width = 2,bg='#9c9c9c')
button_play1.grid(row = 4, column = 4, padx = 5, pady = 10)
                                        # define the start_playing button

button_rewind1 = tkinter.Button(top, text = '⏮', command = lambda: rewind_song(1,volume1.get()/100., master_volume.get()/100., volume5.get()/100.))
button_rewind1.config(height = 1, width = 2,bg='#9c9c9c')
button_rewind1.grid(row = 4, column = 5, padx = 5, pady = 10)
                                        # define the rewind button

volume1_grid=tkinter.Frame(top,bg='#9c9c9c')  
volume1_grid.grid(row=4,column=6)       # define the Frame for volume control of a perticular channel

volume1 = tkinter.DoubleVar()
volume_scale1 = tkinter.Scale(volume1_grid, variable = volume1, orient = tkinter.HORIZONTAL, command = set_volume1)
volume_scale1.grid(row = 1, column = 0, padx = 0, pady = 0)
volume1.set(100)
volume_scale1.config(bg='#9c9c9c')      # define the scale for volume in the Frame defined above
                     
volume1_label = tkinter.Label(volume1_grid, text='Volume channel 1')
volume1_label.grid(row=0, column=0, pady = 0)
volume1_label.config(bg='#9c9c9c')      # define the presenting box of volume information in the Frame defined above

synch1_grid=tkinter.Frame(top,bg='#9c9c9c')
synch1_grid.grid(row=5,column=1)        # define the Frame for synchronising

synchro1_label = tkinter.Label(synch1_grid, text='Synchro with:')
synchro1_label.grid(row=0, column=0, pady = 0)
synchro1_label.config(bg='#9c9c9c')     # define the label to give hints in the Frame defined above

select_synchro1=tkinter.StringVar()
select_synchro1.set("channel __ ")
select_synchro1_box=tkinter.Entry(synch1_grid, width = 11, justify = tkinter.CENTER, textvariable=select_synchro1,bg='#9c9c9c')
select_synchro1_box.grid(row=0, column=1, pady=0)
                                        # define the entry box to input a channel number 
                                        # for this channel to synchronise with in the Frame defined above

volume5_grid=tkinter.Frame(top,bg='#9c9c9c')
volume5_grid.grid(row=5,column=6)       # define the volume Frame for the bass boost of channel1(1+4)

volume5_label = tkinter.Label(volume5_grid, text='Bass boost\nChannel 1')
volume5_label.grid(row=0, column=0, pady = 0)
volume5_label.config(bg='#9c9c9c')      # define the label to give information of bass boost

volume5 = tkinter.DoubleVar()
volumeboost1_scale = tkinter.Scale(volume5_grid, variable = volume5, orient = tkinter.HORIZONTAL, command = set_volume5)
volumeboost1_scale.grid(row = 0, column = 1, padx = 0, pady = 0)
volume5.set(0)
volumeboost1_scale.config(bg='#9c9c9c') # define the scale to control the volume of the bass boost

check_pitch_shift1=tkinter.Frame(top,bg='#9c9c9c')
check_pitch_shift1.grid(row=4,column=7) # define the Frame for pitch information

pitch_value1=tkinter.IntVar()
pitch_scale1=tkinter.Scale(check_pitch_shift1, from_=-12,to=12,resolution=1,orient=tkinter.HORIZONTAL,variable=pitch_value1,bg='#9c9c9c')
pitch_scale1.grid(row=1,column=0)
pitch_value1.set(0)                     # define the scale for pitch in the Frame defined above

if_pitch1=tkinter.BooleanVar()
pitch_button1=tkinter.Checkbutton(check_pitch_shift1,variable=if_pitch1, text="Pitch shift",command=lambda: pitch_selected(1,if_pitch1,pitch_value1),bg='#9c9c9c')
pitch_button1.grid(row=0,column=0)      # define the status_check button that wether a song to switch pitvh
                                        #in the Frame defined above

################################### space between 1&2 #####################################
space_channel12=tkinter.Label(top,bg='#707070')
space_channel12.grid(row=6)
################################### Channel 2 ###################################

select_channel2 = tkinter.Button(top, text = 'Select channel 2', command = lambda: tk_read_file(2),bg='#9c9c9c')
select_channel2.config(height = 1, width = 17)
select_channel2.grid(row = 7, column = 0, padx = 5, pady = 5)
                                        # define the select_file_path button

selected_channel2=tkinter.StringVar()
selected_channel2.set("")
selected_channel2_box=tkinter.Entry(top, width = 30, textvariable=selected_channel2,bg='#9c9c9c')
selected_channel2_box.grid(row=7, column=1, pady=5)
                                        # define the place to present the file path

repeat_bool2=tkinter.BooleanVar()
repeat_bool2.set(False)
check_repeat2=tkinter.Checkbutton(top,variable=repeat_bool2,text="repeat",command=lambda: channel_repeat(2),bg='#9c9c9c')
check_repeat2.grid(row = 7, column = 2, padx = 5, pady = 10)
                                        # define the status confirming checkbox of a song's repeating

button_pause2 = tkinter.Button(top, text = '⏸', command = lambda: pause_unpause(2),bg='#9c9c9c')
button_pause2.config(height = 1, width = 2)
button_pause2.grid(row = 7, column = 3, padx = 5, pady = 10)
                                        # define the pause and unpause button

button_play2 = tkinter.Button(top, text = '▶', command = lambda: play(2, volume2.get()/100., master_volume.get()/100., volume6.get()))
button_play2.config(height = 1, width = 2,bg='#9c9c9c')
button_play2.grid(row = 7, column = 4, padx = 5, pady = 10)
                                        # define the start_playing button

button_rewind2 = tkinter.Button(top, text = '⏮', command = lambda: rewind_song(2,volume2.get()/100., master_volume.get()/100., volume6.get()/100.))
button_rewind2.config(height = 1, width = 2,bg='#9c9c9c')
button_rewind2.grid(row = 7, column = 5, padx = 5, pady = 10)
                                        # define the rewind button

volume2_grid=tkinter.Frame(top,bg='#9c9c9c')  
volume2_grid.grid(row=7,column=6)       # define the Frame for volume control of a perticular channel

volume2 = tkinter.DoubleVar()
volume_scale2 = tkinter.Scale(volume2_grid, variable = volume2, orient = tkinter.HORIZONTAL, command = set_volume2)
volume_scale2.grid(row = 1, column = 0, padx = 0, pady = 0)
volume2.set(100)
volume_scale2.config(bg='#9c9c9c')      # define the scale for volume in the Frame defined above
                     
volume2_label = tkinter.Label(volume2_grid, text='Volume channel 2')
volume2_label.grid(row=0, column=0, pady = 0)
volume2_label.config(bg='#9c9c9c')      # define the presenting box of volume information in the Frame defined above

check_pitch_shift2=tkinter.Frame(top,bg='#9c9c9c')
check_pitch_shift2.grid(row=7,column=7) # define the Frame for pitch information

pitch_value2=tkinter.IntVar()
pitch_scale2=tkinter.Scale(check_pitch_shift2, from_=-12,to=12,resolution=1,orient=tkinter.HORIZONTAL,variable=pitch_value2,bg='#9c9c9c')
pitch_scale2.grid(row=1,column=0)
pitch_value2.set(0)                     # define the scale for pitch in the Frame defined above

if_pitch2=tkinter.BooleanVar()
pitch_button2=tkinter.Checkbutton(check_pitch_shift2,variable=if_pitch2, text="Pitch shift",command=lambda: pitch_selected(2,if_pitch2,pitch_value2),bg='#9c9c9c')
pitch_button2.grid(row=0,column=0)      # define the status_check button that wether a song to switch pitvh
                                        #in the Frame defined above

synch2_grid=tkinter.Frame(top,bg='#9c9c9c')
synch2_grid.grid(row=8,column=1)        # define the Frame for synchronising

synchro2_label = tkinter.Label(synch2_grid, text='Synchro with:')
synchro2_label.grid(row=0, column=0, pady = 0)
synchro2_label.config(bg='#9c9c9c')     # define the label to give hints in the Frame defined above

select_synchro2=tkinter.StringVar()
select_synchro2.set("channel __ ")
select_synchro2_box=tkinter.Entry(synch2_grid, width = 11, justify = tkinter.CENTER, textvariable=select_synchro2,bg='#9c9c9c')
select_synchro2_box.grid(row=0, column=1, pady=0)
                                        # define the entry box to input a channel number 
                                        # for this channel to synchronise with in the Frame defined above

volume6_grid=tkinter.Frame(top,bg='#9c9c9c')
volume6_grid.grid(row=8,column=6)       # define the volume Frame for the bass boost of channel2(2+4)

volume6_label = tkinter.Label(volume6_grid, text='Bass boost\nChannel 2')
volume6_label.grid(row=0, column=0, pady = 0)
volume6_label.config(bg='#9c9c9c')      # define the label to give information of bass boost

volume6 = tkinter.DoubleVar()
volumeboost2_scale = tkinter.Scale(volume6_grid, variable = volume6, orient = tkinter.HORIZONTAL, command = set_volume6)
volumeboost2_scale.grid(row = 0, column = 1, padx = 0, pady = 0)
volume6.set(0)
volumeboost2_scale.config(bg='#9c9c9c') # define the scale to control the volume of the bass boost

button_quit=tkinter.Button(top, text="Split harmonic\npercussive", command=lambda:split_song(file_path2,2),bg='#9c9c9c')
button_quit.config(height = 2, width = 11 )
button_quit.grid(row=7, column=8, padx=20,pady=5)
                                        # define a button to split a song
################################### space between 2&3 #####################################
space_channel23=tkinter.Label(top,bg='#707070')
space_channel23.grid(row=9)
################################### Channel 3 ###################################

select_channel3 = tkinter.Button(top, text = 'Select channel 3', command = lambda: tk_read_file(3))
select_channel3.config(height = 1, width = 17,bg='#9c9c9c')
select_channel3.grid(row = 10, column = 0, padx = 5, pady = 5)
                                        # define the select_file_path button
                                        
selected_channel3=tkinter.StringVar()
selected_channel3.set("")
selected_channel3_box=tkinter.Entry(top, width = 30, textvariable=selected_channel3,bg='#9c9c9c')
selected_channel3_box.grid(row=10, column=1, pady=5)
                                        # define the place to present the file path

repeat_bool3=tkinter.BooleanVar()
repeat_bool3.set(False)
check_repeat3=tkinter.Checkbutton(top,variable=repeat_bool3,text="repeat",command=lambda: channel_repeat(3),bg='#9c9c9c')
check_repeat3.grid(row = 10, column = 2, padx = 5, pady = 10)
                                        # define the status confirming checkbox of a song's repeating

button_pause3 = tkinter.Button(top, text = '⏸', command = lambda: pause_unpause(3))
button_pause3.config(height = 1, width = 2,bg='#9c9c9c')
button_pause3.grid(row = 10, column = 3, padx = 5, pady = 10)
                                        # define the pause and unpause button

button_play3 = tkinter.Button(top, text = '▶', command = lambda: play(3, volume3.get()/100., master_volume.get()/100., volume7.get()))
button_play3.config(height = 1, width = 2,bg='#9c9c9c')
button_play3.grid(row = 10, column = 4, padx = 5, pady = 10)
                                        # define the start_playing button

button_rewind3 = tkinter.Button(top, text = '⏮', command = lambda: rewind_song(3,volume3.get()/100., master_volume.get()/100., volume7.get()/100.))
button_rewind3.config(height = 1, width = 2,bg='#9c9c9c')
button_rewind3.grid(row = 10, column = 5, padx = 5, pady = 10)
                                        # define the rewind button

volume3_grid=tkinter.Frame(top,bg='#9c9c9c')  
volume3_grid.grid(row=10,column=6)      # define the Frame for volume control of a perticular channel

volume3 = tkinter.DoubleVar()
volume_scale3 = tkinter.Scale(volume3_grid, variable = volume3, orient = tkinter.HORIZONTAL, command = set_volume3)
volume_scale3.grid(row = 1, column = 0, padx = 0, pady = 0)
volume3.set(100)
volume_scale3.config(bg='#9c9c9c')      # define the scale for volume in the Frame defined above
                     
volume3_label = tkinter.Label(volume3_grid, text='Volume channel 3')
volume3_label.grid(row=0, column=0, pady = 0)
volume3_label.config(bg='#9c9c9c')      # define the presenting box of volume information in the Frame defined above

check_pitch_shift3=tkinter.Frame(top,bg='#9c9c9c')
check_pitch_shift3.grid(row=10,column=7)# define the Frame for pitch information

pitch_value3=tkinter.IntVar()
pitch_scale3=tkinter.Scale(check_pitch_shift3, from_=-12,to=12,resolution=1,orient=tkinter.HORIZONTAL,variable=pitch_value3,bg='#9c9c9c')
pitch_scale3.grid(row=1,column=0)
pitch_value3.set(0)                     # define the scale for pitch in the Frame defined above

if_pitch3=tkinter.BooleanVar()
pitch_button3=tkinter.Checkbutton(check_pitch_shift3,variable=if_pitch3, text="Pitch shift",command=lambda: pitch_selected(3,if_pitch3,pitch_value3),bg='#9c9c9c')
pitch_button3.grid(row=0,column=0)      # define the status_check button that wether a song to switch pitvh
                                        #in the Frame defined above

synch3_grid=tkinter.Frame(top,bg='#9c9c9c')
synch3_grid.grid(row=11,column=1)       # define the Frame for synchronising

synchro3_label = tkinter.Label(synch3_grid, text='Synchro with:')
synchro3_label.grid(row=0, column=0, pady = 0)
synchro3_label.config(bg='#9c9c9c')     # define the label to give hints in the Frame defined above

select_synchro3=tkinter.StringVar()
select_synchro3.set("channel __ ")
select_synchro3_box=tkinter.Entry(synch3_grid, width = 11, justify = tkinter.CENTER, textvariable=select_synchro3,bg='#9c9c9c')
select_synchro3_box.grid(row=0, column=1, pady=0)
                                        # define the entry box to input a channel number 
                                        # for this channel to synchronise with in the Frame defined above

volume7_grid=tkinter.Frame(top,bg='#9c9c9c')
volume7_grid.grid(row=11,column=6)      # define the volume Frame for the bass boost of channel3(3+4)

volume7_label = tkinter.Label(volume7_grid, text='Bass boost\nChannel 3')
volume7_label.grid(row=0, column=0, pady = 0)
volume7_label.config(bg='#9c9c9c')      # define the label to give information of bass boost

volume7 = tkinter.DoubleVar()
volumeboost3_scale = tkinter.Scale(volume7_grid, variable = volume7, orient = tkinter.HORIZONTAL, command = set_volume7)
volumeboost3_scale.grid(row = 0, column = 1, padx = 0, pady = 0)
volume7.set(0)
volumeboost3_scale.config(bg='#9c9c9c') # define the scale to control the volume of the bass boost
    
################################### space between 3&master #####################################

space_channel3m=tkinter.Label(top,bg='#707070')
space_channel3m.grid(row=12)

################################################################################

button_synchro = tkinter.Button(top, text = 'synchro', command = lambda :synchro(select_synchro0, select_synchro1, select_synchro2, select_synchro3))
button_synchro.config(height = 1, width = 10,bg='#9c9c9c')
button_synchro.grid(row = 13, column = 0, padx = 5, pady = 10)
                                # define a button to pass the synchronising information

button_pause_all = tkinter.Button(top, text = '⏸', command =set_all_pause_unpause,bg='#9c9c9c')
button_pause_all.config(height = 1, width = 2)
button_pause_all.grid(row = 13, column = 2, padx = 5, pady = 10)
                                # define the button to pause or unpause all songs

button_play_all = tkinter.Button(top, text = '▶', command = set_all_start)
button_play_all.config(height = 1, width = 2,bg='#9c9c9c')
button_play_all.grid(row = 13, column =3, padx = 5, pady = 10)
                                # define the button to start all songs

mastervolume_grid=tkinter.Frame(top,bg='#9c9c9c')
mastervolume_grid.grid(row=13,column=1,pady = 10)
                                # define the frame for the master volume module

volume1_label = tkinter.Label(mastervolume_grid, text='Master Volume',bg='#9c9c9c')
volume1_label.grid(row=0, column=0, pady = 0)
                                # define the label to give hints of master volume

master_volume = tkinter.DoubleVar()
master_volume_scale = tkinter.Scale(mastervolume_grid, variable = master_volume, length=175, orient = tkinter.HORIZONTAL, command = set_volumn_all,bg='#9c9c9c')
master_volume_scale.grid(row = 1, column =0 )
master_volume.set(100)
                                # define the scale to control master volume

button_quit=tkinter.Button(top, text="Quit", command=lambda root=root:quit(root))
button_quit.config(height = 2, width = 11 ,bg='#9c9c9c')
button_quit.grid(row=13, column=5, padx=20,pady=5)
                                # define a button to stop all songs and quit the window

button_quit=tkinter.Button(top, text="Save", command=list_songs)
button_quit.config(height = 2, width = 11,bg='#9c9c9c' )
button_quit.grid(row=13, column=6, padx=20,pady=5)
                                # define a button to save the mixed sound to disk

name_save=tkinter.StringVar()
name_save.set("result.wav")
name_save_box=tkinter.Entry(top, width = 20, justify = tkinter.CENTER, textvariable=name_save,bg='#9c9c9c')
name_save_box.grid(row=13, column=7, pady=5)
                                # define an entry box to input the name of the mixed sound
################################# seperation of the controling part from the outside window #####################
volume1_label = tkinter.Label(top, text='',bg='#707070')
volume1_label.grid(row=14, column=0, pady = 40)

volume1_label = tkinter.Label(top, text='',bg='#707070')
volume1_label.grid(row=0, column=9, padx = 5)
###############################################################################

root.mainloop() # Appelle l'event loop