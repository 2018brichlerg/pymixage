import librosa
import numpy as np
import soundfile as sf
import song_play
import filters

def synchro_file(filename1,filename2,n2):                      # a function that synchronises two songs by tracking first beats assuming that they have the same tempo
    y1,sr1=librosa.load(filename1)                             #loading the audio as a time series np array y1 / sr1 represents samplig rate
    tempo1, beats1 = librosa.beat.beat_track(y=y1, sr=sr1)     #estimating the first song tempo and beat events locations
    index1= librosa.frames_to_samples(beats1[0])               # order of first sample matching the first beat in y1
    y2, sr2 = librosa.load(filename2)
    tempo2, beats2 = librosa.beat.beat_track(y=y2, sr=sr2)
    index2= librosa.frames_to_samples(beats2[0])               # order of first sample matching the first beat in y2
    ysilence=np.zeros(index1)                                  # creating a moment of silence befor the first beat in the second song for synchronisation
    yf= np.concatenate((ysilence,y2[index2:]),axis=0)
    index = filename2.rfind('.')
    new_filename2 = filename2[:index]+'_synchronized.wav'      #attributing the filename with its directory
    sf.write(new_filename2, yf, sr2, subtype='PCM_16')         #converting the np.array yf to an audio file
    song_play.load_song(new_filename2,n2)
    song_play.load_song(filters.bass_boost(new_filename2),n2+4)
