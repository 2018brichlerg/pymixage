import librosa
import numpy as np
import matplotlib.pyplot as plt
import soundfile as sf



def remix(path):                                                                       #a function for playing audio files backward
    y, sr = librosa.load(path)
    beat_frames = librosa.beat.beat_track(y=y, sr=sr,hop_length=512)                   #calculating beats
    beat_samples = librosa.frames_to_samples(beat_frames[1],hop_length=512,n_fft=None) #converting frames to samples
    intervals = librosa.util.frame(beat_samples, frame_length=2,hop_length=1).T        #generating intervals from samples
    y_out = librosa.effects.remix(y, intervals[::-1])                                  #reversing intervals
    index = path.rfind('.')
    new_filename = path[:index]+'_remixed.wav'                                   #attributing the filename with its directory
    sf.write(new_filename,y_out , sr, subtype='PCM_16')                                #converting the np.array y_out to an audio file
    return new_filename
