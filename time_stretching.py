import librosa
import numpy as np
import soundfile as sf


def accelerate(filename):                                                #a function that accelerates the audio file reading time
    y, sr = librosa.load(filename)
    y_fast = librosa.effects.time_stretch(y, 2.0)                        #the second parameter refers to stretch factor
    index = filename.rfind('.')
    new_filename = filename[:index]+'_accelerated.wav'                   #attributing the filename with its directory
    sf.write(new_filename,y_fast , sr, subtype='PCM_16')                 #converting the np.array y_fast to an audio file
    return new_filename

def decelerate(filename):                                                #a function that dcecelerates the audio file reading time
    y, sr = librosa.load(filename)
    y_slow = librosa.effects.time_stretch(y, 0.5)                        #the second parameter refers to stretch factor
    index = filename.rfind('.')
    new_filename = filename[:index]+'_pitch_decelerated.wav'             #attributing the filename with its directory
    sf.write(new_filename,y_slow , sr, subtype='PCM_16')                 #converting the np.array y_slow to an audio file
    return new_filename

