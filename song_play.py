import pygame.mixer



def start_player() : #This function starts the song mixer
    pygame.mixer.init()
    pygame.mixer.set_num_channels(10) #Loading 10 channels
    pygame.mixer.set_reserved(0)
    
    
    

def if_loaded(n): #This functions checks if a song is loaded in a channel
    return pygame.mixer.Channel(n).get_busy()

def load_song(song_path, channeln) : #This function loads a song in a channel
    song=pygame.mixer.Sound(song_path)
    channel=pygame.mixer.Channel(channeln)
    channel.play(song)
    channel.pause()

def start_song(n,volumn,sys_volumn,volumn_boost) : #This function plays the song and its bass boosted version
    chan =pygame.mixer.Channel(n)
    chan_boost = pygame.mixer.Channel(n+4)
    if(pygame.mixer.Channel(n).get_busy()):
        chan.set_volume(float(volumn)*float(sys_volumn))
        chan.unpause()
        chan_boost.set_volume(float(volumn_boost)*float(sys_volumn))
        chan_boost.unpause()
        return True
    else:
        return False

def rewind_song(n,volumn,sys_volumn,volumn_boost) : #This function rewinds a song
    pygame.mixer.Channel(n).play(pygame.mixer.Channel(n).get_sound())
    pygame.mixer.Channel(n+4).play(pygame.mixer.Channel(n+4).get_sound())
    start_song(n,volumn,sys_volumn,volumn_boost)

def set_volumn_song(n,volumn,sys_volumn): #This function sets the volume of a channel
    if(pygame.mixer.Channel(n).get_busy()):
        pygame.mixer.Channel(n).set_volume(volumn*sys_volumn)
         
def pause_song(n): #This function pauses a channel and its bass boosted alter ego
    if(pygame.mixer.Channel(n).get_busy()):
        pygame.mixer.Channel(n).pause()
        pygame.mixer.Channel(n+4).pause()
        return True
    else:
        return False
    
def resume_song(n): #This function unpauses a channel and its bass boosted alter ego
    if(pygame.mixer.Channel(n).get_busy()):
        pygame.mixer.Channel(n).unpause()
        pygame.mixer.Channel(n+4).unpause()
        return True
    else:
        return False
    
#def stop_song(n) :
#    pygame.mixer.Channel(n).fadeout(1000)

def pause_all(): #This function pauses all channels
    for i in range(10):
        if(pygame.mixer.Channel(i).get_busy()):
            pygame.mixer.Channel(i).pause()
    
def resume_all(): #This function unpauses all channels
    for i in range(10):
        if(pygame.mixer.Channel(i).get_busy()):
            pygame.mixer.Channel(i).unpause()
    
def stop_all(): #This function stops the song mixer
    pygame.mixer.stop()
    
